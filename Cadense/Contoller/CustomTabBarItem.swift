//
//  MyTabBarItem.swift
//  Cadense
//
//  Created by Lucky on 24/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.


import UIKit
@IBDesignable
class CustomTabBarItem: UITabBarItem {
    override func awakeFromNib() {
        super.awakeFromNib()
        if let image = image{
            self.image = image.withRenderingMode(.alwaysOriginal)
        }
        if let image = selectedImage{
            self.selectedImage = image.withRenderingMode(.alwaysOriginal)
        }
        self.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5.0)
        
        setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
    }
}
