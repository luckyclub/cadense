//
//  BookingDetailVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD

class BookingDetailVC: UIViewController {

    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var phonenumberLbl: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    
    let segueIdentifier = "toLeaveReviewSegue"
    var book: CBooking!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeRoundImageWithGrayBorder(avatarImg)
        initViewsWithBookData()
    }
    
    func initViewsWithBookData() {
        let date = book.date
        dayLbl.text = customSplitDate(date: date!, option: 1)
        monthLbl.text = customSplitDate(date: date!, option: 0)
        
        eventLbl.text = book.type!
        let durationStr = book.duration!.components(separatedBy: ",")
        timeLbl.text = durationStr[0] + " - " + durationStr[1]
        
        Api.CUser.observeUser(withId: book.clientId!) { (user) in
            if let photoUrlString = user.profileImageUrl {
                let photoUrl = URL(string: photoUrlString)
                self.avatarImg.sd_setImage(with: photoUrl)
            }
            
            let fullName: String! = user.username!
            let fullNameArr = fullName.components(separatedBy: " ")
            self.nameLbl.text = "\(fullNameArr[0])'s Catering"
        }
        
        confirmLbl.text = "not confirmed yet"
        if book.isConfirm! {
            confirmLbl.text = "confirmed"
        }
        
        priceLbl.text = book.price!
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onDiscoverDetailAction(_ sender: Any) {
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Discover", bundle: nil)
        let viewcontroller = mainView.instantiateViewController(withIdentifier: "DiscoverDetailVC") as! DiscoverDetailVC
//        UIApplication.shared.keyWindow?.rootViewController = viewcontroller
        self.present(viewcontroller, animated:true, completion:nil)
        
    }
    
    @IBAction func onLeaveReviewAction(_ sender: Any) {
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        //TODO cancel booking
        ProgressHUD.showError("Pending!", interaction: false)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let destinationVC = segue.destination as! LeaveReviewVC
            destinationVC.book = self.book
        }
    }

}
