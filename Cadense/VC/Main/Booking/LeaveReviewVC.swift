//
//  LeaveReviewVC.swift
//  Cadense
//
//  Created by Lucky on 4/20/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD

class LeaveReviewVC: UIViewController, RateViewDelegate {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var feedbackTxtView: UITextView!
    @IBOutlet weak var tableview: UITableView!
    
    var status : Int! = 0
    var reviewTypeAry = [String]()
    var feedbackTitleAry = [String]()
    let cellReuseIdentifier = "leaveReviewCell"
    
    var publicFeedbackStr = ""
    var privateFeedbackStr = ""
    var book: CBooking!
    var cater: CCater!
    var rateAry: [Int] = [0, 0, 0, 0, 0]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reviewTypeAry = ["Food", "Vibe", "Quality of Service", "Responsiveness", "Value"]
        feedbackTitleAry = ["Feedback(publicly visible)", "Feedback(private)", "Anything we should know?"]
        
        makeRoundImageWithGrayBorder(avatarImg)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        if status == 0 {
            self.dismiss(animated: true, completion: nil)
        } else if status == 1 {
            tableview.isHidden = false
            status = 0
        } else if status == 2 {
            typeLbl.text = feedbackTitleAry[0]
            feedbackTxtView.text = publicFeedbackStr
            status = 1
        } else if status == 3 {
            typeLbl.text = feedbackTitleAry[1]
            feedbackTxtView.text = privateFeedbackStr
            nextBtn.setTitle("Next",for: .normal)
            status = 2
        }
    }
    
    @IBAction func onNextAction(_ sender: Any) {
        if status == 0 {
            tableview.isHidden = true
            typeLbl.text = feedbackTitleAry[0]
            feedbackTxtView.text = ""
            feedbackTxtView.becomeFirstResponder()
            status = 1
        } else if status == 1 {
            publicFeedbackStr = feedbackTxtView.text
            typeLbl.text = feedbackTitleAry[1]
            feedbackTxtView.text = ""
            feedbackTxtView.becomeFirstResponder()
            status = 2
        } else if status == 2 {
            privateFeedbackStr = feedbackTxtView.text
            typeLbl.text = feedbackTitleAry[2]
            feedbackTxtView.text = ""
            feedbackTxtView.becomeFirstResponder()
            nextBtn.setTitle("Submit",for: .normal)
            status = 3
        } else if status == 3 {
            uploadMyReview()
        }
    }
    
    func uploadMyReview() {
        //add average rate
        var avRate: Int = 0
        for index in 0...4 {
            avRate += rateAry[index]
        }
        avRate = Int(avRate / 5)
        rateAry.append(avRate)
        
        let dic = [Config.KEY_CATERID : book.caterId!,
                   Config.KEY_CLIENTID: book.clientId!,
                   Config.KEY_USERID  : g_userDefault.string(forKey: g_skCurrentUserKey)!,
                   Config.KEY_BOOKID: book.id!,
                   Config.KEY_URL_IMAGE: book.caterImageUrl!,
                   Config.KEY_RATE: rateAry,
                   Config.KEY_DATE: Date().timeIntervalSince1970,
                   Config.KEY_FD_PUBLIC: publicFeedbackStr,
                   Config.KEY_FD_PRIVATE: privateFeedbackStr,
                   Config.KEY_FD_PROTECT: feedbackTxtView.text!
                   ] as [String : Any]
        
        ProgressHUD.show("Uploading...", interaction: false)
        ReviewApi.setReview(dict: dic, onSuccess: {
            ProgressHUD.dismiss()
            self.dismiss(animated: true, completion: nil)
        }) { (error) in
            ProgressHUD.showError(error)
        }
    }
    
    func finishCosmosRate(_ sender: LeaveReviewTVCell, rate: Int) {
        guard let tappedIndexPath = self.tableview.indexPath(for: sender) else { return }
        rateAry[tappedIndexPath.row] = rate
    }

}

extension LeaveReviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! LeaveReviewTVCell
        cell.titleLbl.text = reviewTypeAry[indexPath.row]
        cell.delegate = self
        
        return cell
    }
}

extension LeaveReviewVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }

}
