//
//  BookingVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import VACalendar

class BookingVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var calendarBgView: UIView!
    
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let appereance = VAMonthHeaderViewAppearance(
                previousButtonImage: #imageLiteral(resourceName: "icn_previous"),
                nextButtonImage: #imageLiteral(resourceName: "icn_next"),
                dateFormat: "LLLL"
            )
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
        }
    }
    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, calendar: defaultCalendar)
            weekDaysView.appearance = appereance
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    var calendarView: VACalendarView!
    
    let cellReuseIdentifier = "BookingCell"
    var booksAry = [CBooking]()
    var selectedIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addCalendarView()
        
        self.fetchMyBooking()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fetchMyBooking() {
        BookingApi.fetchBooksByUserId(id: g_userDefault.string(forKey: g_skCurrentUserKey)!) { (books) in
            self.booksAry = books
            self.tableview.reloadData()
        }
    }
    
    func addCalendarView() {
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: CGRect(
            x: 0,
            y: 0,
            width: calendarBgView.frame.width,
            height: calendarBgView.frame.height
        ), calendar: calendar)
        
        calendarView.showDaysOut = true
        calendarView.selectionStyle = .single
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        calendarView.setup()
        calendarBgView.addSubview(calendarView)
    }
    
    func goDetailVC(_ index: Int) {
        selectedIndex = index
        self.performSegue(withIdentifier: "BookToDetailSegue", sender: self)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookToDetailSegue" {
            let destinationVC = segue.destination as! BookingDetailVC
            destinationVC.book = booksAry[selectedIndex]
        }
    }

}

extension BookingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! BookingTableViewCell
        
        let book = booksAry[indexPath.row]
        //todo day, month
        let date = book.date
        cell.dayLbl.text = customSplitDate(date: date!, option: 1)
        cell.monthLbl.text = customSplitDate(date: date!, option: 0)
        
        Api.CUser.observeUser(withId: book.clientId!) { (client) in
            let fullName: String! = client.username!
            let fullNameArr = fullName.components(separatedBy: " ")
            cell.titleLbl.text = "\(fullNameArr[0])'s Catering"
        }
        
        cell.eventLbl.text = book.type!
        cell.confirmLbl.text = "not confirmed yet"
        if book.isConfirm! {
            cell.confirmLbl.text = "confirmed"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        goDetailVC(indexPath.row)
    }
}

// Calendar extensions
extension BookingVC: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension BookingVC: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
}

extension BookingVC: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .red
        case .unavailable:
            return .lightGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return .blue // not working
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension BookingVC: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        print(dates)
    }
    
}
