//
//  EditProfileVC.swift
//  Cadense
//
//  Created by Lucky on 4/23/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD
import CoreLocation

protocol EditProfileVCDelegate: class {
    func updateUserProfile(isChanged:Bool)
}

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameFld: UITextField!
    @IBOutlet weak var descriptionFld: UITextField!
    @IBOutlet weak var instituteFld: UITextField!
    @IBOutlet weak var experienceFld: UITextField!
    @IBOutlet weak var certificationFld: UITextField!
    @IBOutlet weak var hourlyRateFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var bottomBgView: UIView!
    
    let picker = UIImagePickerController()
    var isChangedAvatar : Bool! = false
    var isUpdatedEmail : Bool! = false
    var user: CUser!
    var flag: Int! = 0
    var currentAddress: String? = "undefined"
    static var delegate: EditProfileVCDelegate?
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        makeRoundImageWithWhiteBorder(avatarImg)
        picker.delegate = self
        
//        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        fillSubviews()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print(location.coordinate)
            currentLocation = location
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fillSubviews() {
        if let photoUrlString = user!.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            avatarImg.sd_setImage(with: photoUrl)
        }
        
        nameFld.text = user.username
        descriptionFld.text = user.description
        instituteFld.text = user.institute
        experienceFld.text = user.experience
        certificationFld.text = user.certification
        hourlyRateFld.text = user.hourlyRate
        emailFld.text = user.email
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func tapAvatarImgAction(_ sender: UITapGestureRecognizer) {
        
        let actionSheet = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "From Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.openGallary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else{
            noCamera()
        }
    }
    
    func noCamera(){
        let alert = UIAlertController(title: "No Camera", message: "This device has no Camera", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        picker.popoverPresentationController?.sourceView = avatarImg
        picker.popoverPresentationController?.sourceRect = avatarImg.bounds;
        picker.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down;
    }
    
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        isChangedAvatar = true
        
         let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
//        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
//        avatarImg.contentMode = .scaleAspectFill        
        avatarImg.image = self.resizeImage(image: chosenImage, targetSize: CGSize(width: 250, height: 250))
        
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
         picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCloseAccountBtnAction(_ sender: Any) {
        let alertVC = UIAlertController(title: "Alert", message: "Do you really want to close your account?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert) -> Void in
            // close account action with Firebase
            self.deleteAccount()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert) -> Void in
            
        })
        alertVC.addAction(cancelAction)
        alertVC.addAction(okAction)
        alertVC.view.tintColor = UIColor.black
        present(alertVC, animated: true)
    }
    
    func deleteAccount() {
        ProgressHUD.show("", interaction: false)
        AuthService.delete(onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.goMainAuthVC()
        }) { (error) in
            ProgressHUD.showError(error!)
        }
    }
    
    func goMainAuthVC() {
        g_userDefault.set(false, forKey: g_skStayLogin)
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Auth", bundle: nil)
        let viewcontroller = mainView.instantiateViewController(withIdentifier: "AuthNavVC") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = viewcontroller
    }
    
    @IBAction func onSaveAction(_ sender: Any) {
        ProgressHUD.show("Updating...", interaction: false)
        
        flag = 0
        
        if isChangedAvatar {
            uploadAvatarImage()
        } else {
            updateFlag()
        }
        
        if emailFld.text != user.email {
            isUpdatedEmail = true
            updateEmail()
        } else {
            updateFlag()
        }
        
        updateUserInfo()
    }
    
    func updateFlag() {
        flag = flag + 1
        checkUploadCompletion()
    }
    
    func uploadAvatarImage() {
        let imageData: Data = UIImagePNGRepresentation(avatarImg.image!)!
        AuthService.updateUserAvatar(imageData: imageData, onSuccess: {
            self.isChangedAvatar = false
            self.updateFlag()
        }, onError: { error in
            ProgressHUD.dismiss()
            ProgressHUD.showError(error!)
        })
    }
    
    func updateEmail() {
        AuthService.updateUserEmail(email: emailFld.text!, onSuccess: {
            self.isUpdatedEmail = true
            self.updateFlag()
        }, onError: { error in
            ProgressHUD.dismiss()
            ProgressHUD.showError(error!)
        })
    }
    
    func updateUserInfo() {
        let mylocation: String? = "\(currentLocation.coordinate.latitude)" + "," + "\(currentLocation.coordinate.longitude)"
        currentAddress = getAddressFromLatLon(location: currentLocation)
        let infoDict = ["username" : nameFld.text,
                    "username_lowercase" : nameFld.text?.lowercased(),
                    "description" : descriptionFld.text,
                    "institute" : instituteFld.text,
                    "experience" : experienceFld.text,
                    "certifications" : certificationFld.text,
                    "hourlyRate" : hourlyRateFld.text,
                    "address" : currentAddress,
                    "location" : mylocation]
        
        AuthService.updateUserInfo(dict: infoDict as! [String : String], onSuccess: {
            self.updateFlag()
        }, onError: { error in
            ProgressHUD.dismiss()
            ProgressHUD.showError(error!)
        })
    }
    
    func checkUploadCompletion() {
        if flag != 3 {
            return
        }
        ProgressHUD.dismiss()
        EditProfileVC.delegate?.updateUserProfile(isChanged: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func getAddressFromLatLon(location: CLLocation) -> String {
        let ceo: CLGeocoder = CLGeocoder()
        var addressString : String = ""
        ceo.reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    
                    print(addressString)
                }
        })
        return addressString
    }
}
