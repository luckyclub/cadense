//
//  MyGalleryVC.swift
//  Cadense
//
//  Created by Lucky on 4/23/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import OpalImagePicker
import ProgressHUD

class MyGalleryVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var allPhotosClView: UICollectionView!
    @IBOutlet weak var allPhotoCountLbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    
    let cellReuseIdentifier = "allPhotosClvCell"
    let cellReuseIdentifier2 = "customGalleryCell"
    
    var user: CUser!
    var photoAry = [CCater]()
    var newPhotoAry = [UIImage]()
    var eventAry = [CEvent]()
    let picker = UIImagePickerController()
    var newAlbueTitle = ""
    var albumTitleAry = [String]()
    var eventPicker: UIPickerView!
    var selectedAlbumIndex: Int = 0
    var cleanedCaterDict = [String:[CCater]]()
    var albumTxtFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        allPhotoCountLbl.text = "\(photoAry.count)"
        picker.delegate = self
        
        eventPicker = UIPickerView()
        eventPicker.dataSource = self
        eventPicker.delegate = self
        
        fetchMyEvent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func fetchMyEvent() {
        ProgressHUD.show("", interaction: false)
        EventApi.fetchEventsByUserId(id: self.user.id!) { (events) in
            ProgressHUD.dismiss()
            if !events.isEmpty {
                self.eventAry = events
                self.albumTitleAry.removeAll()
                for event in events {
                    let type = event.type
                    print("event: \(type!): \(event.id!)")
                    self.albumTitleAry.append(type!)
                    self.cleanedCaterDict[type!] = [CCater]()
                }
                    self.cleanGalleryTypes()
            }
        }
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAllPhotosBtnAction(_ sender: Any) {
        //no need at this point
    }
    
    @IBAction func onCameraActionBtnAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else{
            noCamera()
        }
    }
    
    @IBAction func onPhotoAlbumAction(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func cleanGalleryTypes() {
        if !self.photoAry.isEmpty {
            for cater in photoAry {
                let type = cater.type
                var ary = cleanedCaterDict[type!]
                ary?.append(cater)
                cleanedCaterDict[type!] = ary
            }
        }
        
        self.tableview.reloadData()
    }
    
    func uploadNewEvent() {
        EventApi.setEvent(userId: self.user.id!, type: self.newAlbueTitle, onSuccess: {
//            self.albumTitleAry.append(self.newAlbueTitle)
//            self.cleanedCaterDict[self.newAlbueTitle] = [CCater]()
//            self.tableview.reloadData()
        }) { (error) in
            showOkAlert(title: "Alert", msg: error!, vc: self)
        }
    }
    
//    func uploadNewGalleries(withCompletion completion: () -> Void) {
    func uploadNewGalleries() {
        let photo = newPhotoAry.first
        let newPhoto = resizeImage(image: photo!, minSize: CGSize(width: 750, height: 750))
        let imageData: Data = UIImagePNGRepresentation(newPhoto)!
        ProgressHUD.show("uploading...", interaction: false)
        CaterApi.setCater(imageData: imageData, userId: self.user.id!, type: albumTitleAry[self.selectedAlbumIndex], onSuccess: {
            ProgressHUD.dismiss()
            self.uploadGalleryAfterAction()
        }, onError: { (error) in
            print("upload new gallery error: \(String(describing: error))")
            ProgressHUD.dismiss()
            self.uploadGalleryAfterAction()
        })
    }
    
    func uploadGalleryAfterAction() {
        newPhotoAry.removeFirst()
        if newPhotoAry.count != 0 {
            uploadNewGalleries()
        } else {
            print("loop has finished")
            fetchMyGallery()
        }
    }
    
    func fetchMyGallery() {
        CaterApi.fetchCatersByUserId(id: self.user.id!) { (cater) in
            if !cater.isEmpty {
                self.photoAry.removeAll()
                self.photoAry.append(contentsOf:cater)
                self.allPhotoCountLbl.text = String(self.photoAry.count)
                self.allPhotosClView.reloadData()
                self.cleanGalleryTypes()
            }
        }
    }
    
    @IBAction func addNewAlbumAction(_ sender: Any) {
        let alertVC = UIAlertController(title: "Create new album", message: nil, preferredStyle: .alert)
        alertVC.addTextField { (textField) in
            textField.placeholder = "Fundraiser"
        }
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert) -> Void in
            
            let titleTextField = alertVC.textFields![0] as UITextField
            self.newAlbueTitle = titleTextField.text!
            self.uploadNewEvent()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert) -> Void in
            
        })
        alertVC.addAction(cancelAction)
        alertVC.addAction(okAction)
        alertVC.view.tintColor = UIColor.black
        present(alertVC, animated: true)
    }
    
    func selectAlbum() {
        if self.albumTitleAry.count == 0 {
            let alert = UIAlertController(title: "", message: "First, You need to add an album at least.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        } else {
            let alertVC = UIAlertController(title: "", message: "Choose the album that you want to add.", preferredStyle: .alert)
            alertVC.addTextField { (textField) in
                textField.inputView = self.eventPicker
                textField.text = self.albumTitleAry[0]
                self.selectedAlbumIndex = 0
                textField.becomeFirstResponder()
                self.albumTxtFld = textField
            }
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert) -> Void in
                self.uploadNewGalleries()
//                self.uploadNewGalleries(withCompletion: self.uploadGalleryAfterAction)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert) -> Void in
                
            })
            alertVC.addAction(cancelAction)
            alertVC.addAction(okAction)
            alertVC.view.tintColor = UIColor.black
            present(alertVC, animated: true)
        }
    }
    
    func updateAllPhotosWithCountLabel() {
        allPhotoCountLbl.text = "\(photoAry.count)"
        allPhotosClView.reloadData()
    }
    
    func noCamera(){
        let alert = UIAlertController(title: "No Camera", message: "This device has no Camera", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, minSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = minSize.width  / size.width
        let heightRatio = minSize.height / size.height
        
        if widthRatio >= 1.0 && heightRatio >= 1.0 {
            return image
        }
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //UIImagePickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        newPhotoAry.append(chosenImage)
        dismiss(animated:true, completion: nil)
        self.selectAlbum()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //PickerView DataSource methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return albumTitleAry.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return albumTitleAry[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.selectedAlbumIndex = row
        self.albumTxtFld.text = albumTitleAry[self.selectedAlbumIndex]
    }
}

extension MyGalleryVC: OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        //Save Images, update UI
        newPhotoAry.append(contentsOf:images)
        picker.dismiss(animated: true, completion: nil)
        self.selectAlbum()
    }
}

extension MyGalleryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! UserGalleriesCollectionViewCell
        let cater = photoAry[indexPath.item]
        if let photoUrlString = cater.imageUrl {
            let photoUrl = URL(string: photoUrlString)
            cell.galleryPhoto.sd_setImage(with: photoUrl)
        }
        
        return cell
    }
}

extension MyGalleryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumTitleAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier2, for: indexPath) as! CustomGalleryTVCell
        
        let type = albumTitleAry[indexPath.row]
        cell.typeLbl.text = type
        let ary: [CCater] = self.cleanedCaterDict[type]!
        if !(ary.isEmpty) {
            cell.setGalleries(ary: ary)
        }
        cell.countLbl.text = "\(ary.count)"
        
        return cell
    }
}
