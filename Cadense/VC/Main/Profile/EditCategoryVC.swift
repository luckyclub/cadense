//
//  EditCategoryVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

protocol EditCategoryVCDelegate: class {
    func selecteCategory(_ ary: [String])
}

class EditCategoryVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var categoryAry = [String]()
    var selectedAry = [String]()
    let cellReuseIdentifier = "myCategoryTableViewCell"
    static var delegate: EditCategoryVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categoryAry = ["People", "Caterer", "Chef", "Bakery"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSelectAction(_ sender: Any) {
        AuthService.setUserCategory(category: selectedAry, uid: g_userDefault.string(forKey: g_skCurrentUserKey)!, onSuccess: {
            EditCategoryVC.delegate?.selecteCategory(self.selectedAry)
            self.dismiss(animated: true, completion: nil)
        })
    }
}

extension EditCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ChooseCategoryTableViewCell
        
        let title  = categoryAry[indexPath.row]
        cell.categoryName?.text = title
        cell.categoryIcon?.image = UIImage(named:"icon_category_\(title)")
        cell.isCheckedImage?.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! ChooseCategoryTableViewCell
        let title = currentCell.categoryName?.text ?? ""
        if let isChecked = currentCell.isCheckedImage?.isHidden {
            if isChecked {
                currentCell.isCheckedImage?.isHidden = false
                selectedAry.append(title)
            } else {
                currentCell.isCheckedImage?.isHidden = true
                selectedAry = selectedAry.filter() { $0 != title }
            }
        }
    }
}
