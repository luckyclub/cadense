//
//  MyReviewVC.swift
//  Cadense
//
//  Created by Lucky on 5/8/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class MyReviewVC: UIViewController {

    @IBOutlet weak var starImg: UIImageView!
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var foodImg: UIImageView!
    @IBOutlet weak var vibeImg: UIImageView!
    @IBOutlet weak var qualityImg: UIImageView!
    @IBOutlet weak var responseImg: UIImageView!
    @IBOutlet weak var valueImg: UIImageView!
    
    let cellReuseIdentifier = "reviewCell"
    var reviewAry = [CReview]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if reviewAry.count > 0 {
            calculateAverageRate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func calculateAverageRate() {
        let myGroup = DispatchGroup()
        var totalRateAry = [0, 0 , 0 , 0 , 0]
        var avRateAry = [Int]()
        
        for review in reviewAry {
            myGroup.enter()
            let rate:[Int]! = review.rate
            for index in 0...4 {
                totalRateAry[index] += rate[index]
            }
        }
        
        for i in 0...4 {
            myGroup.enter()
            let value = Int(totalRateAry[i] / self.reviewAry.count)
            avRateAry.append(value)
        }
        
        self.initAverageRateView(rate: avRateAry)
        myGroup.notify(queue: .main) {
            print("calculated rate!")
        }
    }
    
    func initAverageRateView(rate: [Int]) {
        foodImg.image = UIImage(named: "icon_star_\(rate[0])")
        vibeImg.image = UIImage(named: "icon_star_\(rate[1])")
        qualityImg.image = UIImage(named: "icon_star_\(rate[2])")
        responseImg.image = UIImage(named: "icon_star_\(rate[3])")
        valueImg.image = UIImage(named: "icon_star_\(rate[4])")
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onStarAction(_ sender: Any) {
        
    }
}

extension MyReviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ReviewTableViewCell
        
        let review = reviewAry[indexPath.row]
        let userid = review.userId
        Api.CUser.observeUser(withId: userid!) { (customer) in
            cell.nameLbl.text = customer.username
            if let avatarUrlString = customer.profileImageUrl {
                let photoUrl = URL(string: avatarUrlString)
                cell.avatarImg.sd_setImage(with: photoUrl)
            }
        }
        cell.reviewLbl.text = review.publicFeedback
        if let photoUrlString = review.caterImageUrl {
            let photoUrl = URL(string: photoUrlString)
            cell.thumbImg.sd_setImage(with: photoUrl)
        }
        
        //need to update returning date such as "2 weeks ago"
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        cell.dateLbl.text = dateFormatter.string(from: review.date!)
        //////////
        cell.delegate = self
        
        return cell
    }
}

extension MyReviewVC: ReviewCellDelegate {
    func viewFullReview(_ sender: ReviewTableViewCell) {
        guard let tappedIndexPath = self.tableview.indexPath(for: sender) else { return }
        print("See full review button clicked!- \(tappedIndexPath.row)")
    }
}

