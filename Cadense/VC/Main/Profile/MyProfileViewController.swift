//
//  MyProfileViewController.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD
import SDWebImage

class MyProfileViewController: UIViewController {
    
    //main view
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var pageInfoLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var certLbl: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var countGalleryLbl: UILabel!
    @IBOutlet weak var reviewCollectionView: UICollectionView!
    
    let cellReuseIdentifier = "myGalleryCollectionViewCell"
    let cellReuseIdentifier2 = "myReviewsCollectionViewCell"
    
    var user: CUser!
    
    var eventAry = [CEvent]()
    var galleryAry = [CCater]()
    var reviewAry = [CReview]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeRoundImageWithWhiteBorder(avatarImg)
        
        EditCategoryVC.delegate = self
        EditProfileVC.delegate = self
        
        //fetch user information
        fetchUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fetchUser() {
        ProgressHUD.show("Loading...", interaction: false)
        
        Api.CUser.observeCurrentUser { (user) in
            self.user = user
            print("userID = \(String(describing: user.id!))")
            
            ProgressHUD.dismiss()
            self.fillSubviews()
            
            self.fetchMyGallery()
            self.fetchMyReviews()
        }
    }
    
    func fetchMyGallery() {
        CaterApi.fetchCatersByUserId(id: self.user.id!) { (cater) in
            if !cater.isEmpty {
                self.galleryAry = cater
                
                //update gallery count lable and reload galleryCollectionView
                self.countGalleryLbl.text = String(self.galleryAry.count)
                self.galleryCollectionView.reloadData()
            }
        }
    }
    
    func fetchMyReviews() {
        ReviewApi.fetchReviewsByClientId(id: self.user.id!) { (reviews) in
            if !reviews.isEmpty {
                self.reviewAry = reviews
                self.reviewCollectionView.reloadData()
            }
        }
    }
    
    func fillSubviews() {
        //set avatarImg
        if let photoUrlString = user!.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            avatarImg.sd_setImage(with: photoUrl)
        }
        
        userNameLbl.text = user.username
        pageInfoLbl.text = user.description
        
        if user.institute! != "" &&  user.experience! != "" {
            bioLbl.text = user.institute! + " - " + user.experience!
        } else {
            bioLbl.text = user.institute! + user.experience!
        }
        
        if user.certification! != "" &&  user.hourlyRate! != "" {
            certLbl.text = user.certification! + " - " + user.hourlyRate!
        } else {
            certLbl.text = user.certification! + user.hourlyRate!
        }
        
        let ary = user.category
        if ary?.isEmpty == false {
            addCategories(ary!)
        }
        
    }
    
    func addCategories(_ ary: [String]) {
        //remove all subviews
        categoryView.subviews.forEach({ $0.removeFromSuperview() })
        
        //add category items
        for i in (0..<ary.count).reversed()
        {
            let ctTitle = ary[i]
            let itemView = UIView(frame: CGRect(x: 24 + i * 62, y: 0, width: 62, height: 62))
            categoryView.addSubview(itemView)
            
            let iconImg = UIImageView(frame: CGRect(x: 16, y: 6, width: 30, height: 30))
            iconImg.image = UIImage(named:"icon_category_\(ctTitle)")
            itemView.addSubview(iconImg)
            
            let ctLbl = UILabel(frame: CGRect(x: 0, y: 41, width: 62, height: 21))
            ctLbl.text = ctTitle
            ctLbl.textAlignment = .center
            ctLbl.textColor = UIColor.darkText
            ctLbl.font = UIFont(name: "Raleway", size: 14)
            itemView.addSubview(ctLbl)
            
        }
    }
    
    @IBAction func onViewAllGalleryAction(_ sender: Any) {
        self.performSegue(withIdentifier: "goMyGallerySegue", sender: self)
    }
    
    @IBAction func onViewAllReviewsAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toMyReviewSegue", sender: self)
    }
    
    @IBAction func onEditProfileAction(_ sender: Any) {
        self.performSegue(withIdentifier: "goEditProfileSegue", sender: self)
    }
    
    @IBAction func onEditCategoryAction(_ sender: Any) {
        self.performSegue(withIdentifier: "profileToCategorySegue", sender: self)
    }
    
    @IBAction func onTapAvatarImgAction(_ sender: Any) {
        // no necessary this method
    }
    
  
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goEditProfileSegue" {
            let destinationVC = segue.destination as! EditProfileVC
            destinationVC.user = self.user
        } else if segue.identifier == "goMyGallerySegue" {
            let destinationVC = segue.destination as! MyGalleryVC
            destinationVC.user = self.user
            destinationVC.photoAry = self.galleryAry
        } else if segue.identifier == "toMyReviewSegue" {
            let destinationVC = segue.destination as! MyReviewVC
            destinationVC.reviewAry = self.reviewAry
        }
    }

}

extension MyProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView ==  galleryCollectionView {
            return galleryAry.count
        }
        return reviewAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView ==  galleryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! UserGalleriesCollectionViewCell
            
            let cater = galleryAry[indexPath.item]
            if let photoUrlString = cater.imageUrl {
                let photoUrl = URL(string: photoUrlString)
                cell.galleryPhoto.sd_setImage(with: photoUrl)
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier2, for: indexPath) as! UserReviewsCollectionViewCell
            
            let review = reviewAry[indexPath.item]
            if let photoUrlString = review.caterImageUrl {
                let photoUrl = URL(string: photoUrlString)
                cell.thumbImg.sd_setImage(with: photoUrl)
            }
            cell.descriptionLbl.text = review.publicFeedback
            cell.delegate = self
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == galleryCollectionView {
            self.performSegue(withIdentifier: "goMyGallerySegue", sender: self)
        }
    }
    
}

extension MyProfileViewController: EditCategoryVCDelegate {
    func selecteCategory(_ ary: [String]) {
        addCategories(ary)
    }
}

extension MyProfileViewController: EditProfileVCDelegate {
    func updateUserProfile(isChanged:Bool) {
        if isChanged {
            fetchUser()
        }
    }
}

extension MyProfileViewController: ReviewClCellDelegate {
    func viewFullReview(_ sender: UserReviewsCollectionViewCell) {
        guard let tappedIndexPath = self.reviewCollectionView.indexPath(for: sender) else { return }
        print("See full review button clicked!- \(tappedIndexPath.row)")
    }
}
