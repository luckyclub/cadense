//
//  SavedVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class SavedVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var foodAry = [[String:String]]()
    let cellReuseIdentifier = "SavedCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        foodAry = [["title": "Vegan Chefs", "count": "24"], ["title": "Bridal Shower", "count": "11"], ["title": "Italian Food", "count": "7"]]
        
        //get data from server
        getSavedData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getSavedData() {
        //here call api
        LikeApi.fetchLikesByUserId(id: g_userDefault.string(forKey: g_skCurrentUserKey)!) { (likes) in
            print("================")
            print(likes)
        }
    }
    
}

extension SavedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SavedTableViewCell
        
        let title  = foodAry[indexPath.row]["title"] ?? ""
        let countText  = foodAry[indexPath.row]["count"] ?? ""
        cell.titleLbl?.text = title
        cell.countLbl?.text = countText + " Photos"
        cell.foodImg1?.image = UIImage(named: "img_temp_food")
        cell.foodImg2?.image = UIImage(named: "img_temp_food1")
        cell.foodImg3?.image = UIImage(named: "img_temp_food2")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
