//
//  ReservationTimeVC.swift
//  Cadense
//
//  Created by Lucky on 4/20/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class ReservationTimeVC: UIViewController {
    
    var selectedDate: Date!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var fromTxfld: UITextField!
    @IBOutlet weak var untilTxfld: UITextField!
    
    var selectedTxfld: UITextField!
    let segueIdentifier = "toReservationResultSegue"
    var cardNumber : String! = "0000"
    var price : String! = "$ 0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(selectedDate)
        addTimePicker()
    }
    
    func addTimePicker() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        fromTxfld.inputView = datePicker
        untilTxfld.inputView = datePicker
        datePicker.addTarget(self, action: #selector(onDate), for: .valueChanged)
    }
    
    @objc func onDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        
        if selectedTxfld == fromTxfld {
            fromTxfld.text = dateFormatter.string(from: sender.date)
        } else {
            untilTxfld.text = dateFormatter.string(from: sender.date)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNextAction(_ sender: Any) {
        //TODO payment gateway
        //set cardNumber and price
        
        //After that;
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let destinationController = segue.destination as! ReservationResultVC
            destinationController.bookingDate = selectedDate!
            destinationController.bookingbeginTime = fromTxfld.text
            destinationController.bookingendTime = untilTxfld.text
            destinationController.cardNumber = cardNumber
            destinationController.price = price
        }
    }

}

extension ReservationTimeVC: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTxfld = textField
        return true
    }
}

