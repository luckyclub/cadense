//
//  DiscoverDetailVC.swift
//  Cadense
//
//  Created by Lucky on 30/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD
import SDWebImage

class DiscoverDetailVC: UIViewController {
    
    //top view
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var starImg: UIImageView!
    
    //main view
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var pageInfoLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var certLbl: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var countGalleryLbl: UILabel!
    @IBOutlet weak var reviewCollectionView: UICollectionView!
    @IBOutlet weak var locationLbl: UILabel!
    
    let cellReuseIdentifier = "UserGalleryCollectionViewCell"
    let cellReuseIdentifier2 = "UserReviewsCollectionViewCell"
    
    var user: CUser!
    var mainCater: CCater!
    var galleryAry = [CCater]()
    var reviewAry = [CReview]()
    var firstname: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchUserInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fetchUserInfo() {
        let userid = mainCater.userId
        ProgressHUD.show("Loading...", interaction: false)
        Api.CUser.observeUser(withId: userid!) { (owner) in
            self.user = owner
            ProgressHUD.dismiss()
            self.fillSubviews()
            
            self.fetchUserGallery()
            self.fetchUserReviews()
        }
    }
    
    func fetchUserGallery() {
        CaterApi.fetchCatersByUserId(id: self.user.id!) { (cater) in
            if !cater.isEmpty {
                self.galleryAry = cater
                
                self.countGalleryLbl.text = String(self.galleryAry.count)
                self.galleryCollectionView.reloadData()
            }
        }
    }
    
    func fetchUserReviews() {
        ReviewApi.fetchReviewsByClientId(id: self.user.id!) { (reviews) in
            if !reviews.isEmpty {
                self.reviewAry = reviews
                self.reviewCollectionView.reloadData()
            }
        }
    }
    
    func fillSubviews() {
        //set avatarImg
        if let photoUrlString = user!.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            avatarImg.sd_setImage(with: photoUrl)
        }
        
        let fullName: String! = user.username
        let fullNameArr = fullName.components(separatedBy: " ")
        firstname = fullNameArr[0]
        nameLbl.text = "\(firstname!)'s Catering"
        userNameLbl.text = fullName
        pageInfoLbl.text = user.description
        
        if user.institute! != "" &&  user.experience! != "" {
            bioLbl.text = user.institute! + " - " + user.experience!
        } else {
            bioLbl.text = user.institute! + user.experience!
        }
        
        if user.certification! != "" &&  user.hourlyRate! != "" {
            certLbl.text = user.certification! + " - " + user.hourlyRate!
        } else {
            certLbl.text = user.certification! + user.hourlyRate!
        }
        
        let ary = user.category
        if ary?.isEmpty == false {
            addCategories(ary!)
        }
        
        locationLbl.text = user.address!        
    }
    
    func addCategories(_ ary: [String]) {
        //remove all subviews
        categoryView.subviews.forEach({ $0.removeFromSuperview() })
        
        //add category items
        for i in (0..<ary.count).reversed()
        {
            let ctTitle = ary[i]
            let itemView = UIView(frame: CGRect(x: 24 + i * 62, y: 0, width: 62, height: 62))
            categoryView.addSubview(itemView)
            
            let iconImg = UIImageView(frame: CGRect(x: 16, y: 6, width: 30, height: 30))
            iconImg.image = UIImage(named:"icon_category_\(ctTitle)")
            itemView.addSubview(iconImg)
            
            let ctLbl = UILabel(frame: CGRect(x: 0, y: 41, width: 62, height: 21))
            ctLbl.text = ctTitle
            ctLbl.textAlignment = .center
            ctLbl.textColor = UIColor.darkText
            ctLbl.font = UIFont(name: "Raleway", size: 14)
            itemView.addSubview(ctLbl)
            
        }
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onStarAction(_ sender: Any) {
        
    }
    
    @IBAction func onMessageAction(_ sender: Any) {
        MessageVC.isSelected = true
        MessageVC.other = self.user
        goMainTabbarVC(3)
    }
    
    @IBAction func onBookAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toReservationSegue", sender: self)
    }
    
    @IBAction func onViewAllGalleryAction(_ sender: Any) {
        gotoGalleryListVC()
    }
    
    @IBAction func onViewAllReviewsAction(_ sender: Any) {
        self.performSegue(withIdentifier: "discoverToReviewSegue", sender: self)
    }
    
    @IBAction func onViewMapAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toLocationSegue", sender: self)
    }
    
    func gotoGalleryListVC() {
        self.performSegue(withIdentifier: "discoverToGalleryListSegue", sender: self)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "discoverToGalleryListSegue" {
            let destinationVC = segue.destination as! GalleryListVC
            destinationVC.user = self.user
            destinationVC.photoAry = self.galleryAry
        } else if segue.identifier == "discoverToReviewSegue" {
            let destinationVC = segue.destination as! ReviewDetailVC
            destinationVC.user = self.user
            destinationVC.reviewAry = self.reviewAry
        } else if segue.identifier == "toLocationSegue" {
            let destinationVC = segue.destination as! UserLocationVC
            destinationVC.locationStr = self.user.location
            destinationVC.firstname = self.firstname!
        }
    } //toLocationSegue

}

extension DiscoverDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView ==  galleryCollectionView {
            return galleryAry.count
        }
        return reviewAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView ==  galleryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! UserGalleriesCollectionViewCell
            let cater = galleryAry[indexPath.item]
            if let photoUrlString = cater.imageUrl {
                let photoUrl = URL(string: photoUrlString)
                cell.galleryPhoto.sd_setImage(with: photoUrl)
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier2, for: indexPath) as! UserReviewsCollectionViewCell
            
            let review = reviewAry[indexPath.item]
            if let photoUrlString = review.caterImageUrl {
                let photoUrl = URL(string: photoUrlString)
                cell.thumbImg.sd_setImage(with: photoUrl)
            }
            cell.descriptionLbl.text = review.publicFeedback
            cell.delegate = self
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView ==  galleryCollectionView {
            gotoGalleryListVC()
        }
    }
    
}

extension DiscoverDetailVC: ReviewClCellDelegate {
    func viewFullReview(_ sender: UserReviewsCollectionViewCell) {
        guard let tappedIndexPath = self.reviewCollectionView.indexPath(for: sender) else { return }
        print("See full review button clicked!- \(tappedIndexPath.row)")
    }
}
