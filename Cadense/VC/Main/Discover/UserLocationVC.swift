//
//  UserLocationVC.swift
//  Cadense
//
//  Created by Lucky on 5/10/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import MapKit

class UserLocationVC: UIViewController {

    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var map: MKMapView!
    var locationStr: String!
    var firstname: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLbl.text = "\(firstname!)'s Location"
        
        let positionArr = locationStr.components(separatedBy: ",")
        let location = CLLocation(latitude: Double(positionArr[0])!, longitude: Double(positionArr[1])!)
        
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        map.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        annotation.title = "\(firstname!)'s Location"
        map.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//extension UserLocationVC: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        guard let annotation = annotation as? Artwork else { return nil }
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            // 5
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = true
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
//}
