//
//  GalleryEventVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import GreedoLayout

class GalleryEventVC: UIViewController, GreedoCollectionViewLayoutDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    //top view
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var starImg: UIImageView!
    
    @IBOutlet weak var eventCollectionView: UICollectionView!
    var collectionViewSizeCalculator: GreedoCollectionViewLayout!
    
    let cellReuseIdentifier = "EventCell"
    
    var eventAry = [CCater]()
    var user: CUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewSizeCalculator = GreedoCollectionViewLayout.init(collectionView: eventCollectionView)
        collectionViewSizeCalculator.rowMaximumHeight = 180.0
        collectionViewSizeCalculator.fixedHeight = true
        
        let fullName: String! = user.username
        let fullNameArr = fullName.components(separatedBy: " ")
        nameLbl.text = "\(fullNameArr[0])'s Catering"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onStarAction(_ sender: Any) {
        
    }
    
    @IBAction func onMessageAction(_ sender: Any) {
        MessageVC.isSelected = true
        MessageVC.other = self.user
        goMainTabbarVC(3)
    }
    
    @IBAction func onBookAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toReservationSegue", sender: self)
    }
    
    // MARK: - CollectionView Delegate methods
    
//    func numberOfColumnsInCollectionView(collectionView: UICollectionView!) -> UInt {
//        return 2
//    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionViewSizeCalculator.sizeForPhoto(at: indexPath)
    }
    
    func greedoCollectionViewLayout(_ layout: GreedoCollectionViewLayout!, originalImageSizeAt indexPath: IndexPath!) -> CGSize {
        
        //TODO here return image size, else
        
        return CGSize(width:0.1 , height:0.1)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("eventary_count: \(eventAry.count)")
        return eventAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! UserGalleriesCollectionViewCell
        
        let cater = eventAry[indexPath.row]
        if let photoUrlString = cater.imageUrl {
            let photoUrl = URL(string: photoUrlString)
            cell.galleryPhoto.sd_setImage(with: photoUrl)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("index: \(indexPath.item)")
    }

}

