//
//  GalleryListVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD

class GalleryListVC: UIViewController {

    //top view
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var starImg: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    
    let cellReuseIdentifier = "galleryListCell"
    
    var user: CUser!
    var photoAry = [CCater]()
    var albumTitleAry = [String]()
    var cleanedCaterDict = [String:[CCater]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fullName: String! = user.username
        let fullNameArr = fullName.components(separatedBy: " ")
        nameLbl.text = "\(fullNameArr[0])'s Catering"
        
        fetchMyEvent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func fetchMyEvent() {
        ProgressHUD.show("", interaction: false)
        EventApi.fetchEventsByUserId(id: self.user.id!) { (events) in
            ProgressHUD.dismiss()
            if !events.isEmpty {
                self.albumTitleAry.removeAll()
                for event in events {
                    let type = event.type
                    print("event: \(type!)")
                    if type == "Event" {
                        self.albumTitleAry.insert(type!, at: 0)
                    }else {
                        self.albumTitleAry.append(type!)
                    }
                    self.cleanedCaterDict[type!] = [CCater]()
                }
                self.cleanGalleryTypes()
            }
        }
    }
    
    func cleanGalleryTypes() {
        if !self.photoAry.isEmpty {
            for cater in photoAry {
                let type = cater.type
                var ary = cleanedCaterDict[type!]
                ary?.append(cater)
                cleanedCaterDict[type!] = ary
            }
        }
        
        self.tableview.reloadData()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onStarAction(_ sender: Any) {
        
    }
    
    @IBAction func onMessageAction(_ sender: Any) {
        MessageVC.isSelected = true
        MessageVC.other = self.user
        goMainTabbarVC(3)
    }
    
    @IBAction func onBookAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toReservationSegue", sender: self)
    }

    func gotoGalleryEventVC() {
        self.performSegue(withIdentifier: "galleryListToEventSegue", sender: self)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "galleryListToEventSegue" {
            let destinationVC = segue.destination as! GalleryEventVC
            let ary: [CCater] = self.cleanedCaterDict["Event"]!
            destinationVC.user = self.user
            destinationVC.eventAry = ary
        }
    }
}

extension GalleryListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumTitleAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustomGalleryTVCell
        
        let type = albumTitleAry[indexPath.row]
        cell.typeLbl.text = type
        let ary: [CCater] = self.cleanedCaterDict[type]!
        if !(ary.isEmpty) {
            cell.setGalleries(ary: ary)
        }
        cell.countLbl.text = "\(ary.count)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = albumTitleAry[indexPath.row]
        if type == "Event" {
            gotoGalleryEventVC()
        }
    }
}
