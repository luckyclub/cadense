//
//  ChooseLocationVC.swift
//  Cadense
//
//  Created by Lucky on 30/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import HNKGooglePlacesAutocomplete
import MapKit

class ChooseLocationVC: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    var searchQuery : HNKGooglePlacesAutocompleteQuery?
    var searchResult = Array<HNKGooglePlacesAutocompletePlace>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchQuery = HNKGooglePlacesAutocompleteQuery.shared()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellLocation")
        let thisPlace = searchResult[indexPath.row]
        cell?.textLabel?.text = thisPlace.name;
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        let selectedPlace = searchResult[indexPath.row]
        
        CLPlacemark.hnk_placemark(fromGooglePlace: selectedPlace, apiKey: searchQuery?.apiKey) { (placemark, addressString, error) in
            if placemark != nil && error == nil{
                tableView.isHidden = true
                tableView.deselectRow(at: indexPath, animated: true)
                g_searchFilter.locationRange = selectedPlace.name
                g_searchFilter.city = placemark?.locality ?? ""
                g_searchFilter.country = placemark?.country ?? ""
                g_searchFilter.street = placemark?.thoroughfare ?? ""
                g_searchFilter.location = (placemark?.location)!
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: g_skUpdateSearchFilter), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0{
            tableView.isHidden = false
            searchQuery? .fetchPlaces(forSearch: searchText, completion: { (places, error) in
                if error != nil {
                    print("Error: " + (error?.localizedDescription)!)
                    showOkAlert(title: "Error", msg: (error?.localizedDescription)!, vc: self)
                }else{
                    self.searchResult = places! as! [HNKGooglePlacesAutocompletePlace]
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        tableView.isHidden = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
