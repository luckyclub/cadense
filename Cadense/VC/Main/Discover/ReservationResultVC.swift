//
//  ReservationResultVC.swift
//  Cadense
//
//  Created by Lucky on 4/20/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import ProgressHUD

class ReservationResultVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    var bookingDate:      Date!
    var bookingbeginTime: String!
    var bookingendTime:   String!
    var cardNumber: String!
    var price: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateStr = getDateString(bookingDate)
        dateLbl.text = dateStr
        timeLbl.text = bookingbeginTime + " - " + bookingendTime
        paymentLbl.text = "visa ****" + cardNumber
        priceLbl.text = price
        
        let cater = g_userDefault.dictionary(forKey: g_skCurrentCater)!
        eventLbl.text = cater[Config.KEY_TYPE]! as? String
        
        Api.CUser.observeCurrentUser { (user) in
            let fullName: String! = user.username
            let fullNameArr = fullName.components(separatedBy: " ")
            self.titleLbl.text = "Book \(fullNameArr[0])'s Catering"
        }
    }
    
    func getDateString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        let str = dateFormatter.string(from: date)
        return str
    }    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNextAction(_ sender: Any) {
        let cater = g_userDefault.dictionary(forKey: g_skCurrentCater)!
        
        let dic = [Config.KEY_CATERID : cater[Config.KEY_ID]!,
                   Config.KEY_CLIENTID: cater[Config.KEY_USERID]!,
                   Config.KEY_URL_IMAGE: cater[Config.KEY_URL_IMAGE]!,
                   Config.KEY_USERID  : g_userDefault.string(forKey: g_skCurrentUserKey)!,
                   Config.KEY_ISCONFIRM: false,
                   Config.KEY_DATE: bookingDate!.timeIntervalSince1970,
                   Config.KEY_DURATION: bookingbeginTime + "," + bookingendTime,
                   Config.KEY_PRICE: price!,
                   Config.KEY_TYPE: cater[Config.KEY_TYPE]!,
                   ] as [String : Any]
        
        ProgressHUD.show("Uploading...", interaction: false)
        BookingApi.setBook(dict: dic, onSuccess: {
            //go booking screen
            ProgressHUD.dismiss()
            goMainTabbarVC(2)
        }) { (error) in
            ProgressHUD.dismiss()
            ProgressHUD.showError(error)
        }
    }

}
