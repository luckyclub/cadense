//
//  DiscoverVC.swift
//  Cadense
//
//  Created by Lucky on 30/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import GooglePlaces
import ProgressHUD
import SDWebImage

class DiscoverVC: UIViewController {
    @IBOutlet weak var viewSearchFilter: UIView!
    @IBOutlet weak var tfSearchDisplay: UITextField!
    
    @IBOutlet weak var tblvDiscover: UITableView!
    
    @IBOutlet weak var lblNoSearchResult: UILabel!
    @IBOutlet weak var btnChooseLocation: UIButton!
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var tfSelectDate: UITextField!
    @IBOutlet weak var btnChooseCategory: UIButton!
    
    @IBOutlet weak var discoverTableview: UITableView!
    @IBOutlet weak var totalRecommendLbl: UILabel!
    @IBOutlet weak var recmdCollectionView: UICollectionView!
    @IBOutlet weak var otherTitleLbl: UILabel!
    @IBOutlet weak var totalOtherLbl: UILabel!
    @IBOutlet weak var otherCollectionView: UICollectionView!
    @IBOutlet weak var searchFilterPopView: UIView!
    
    
    let cellReuseIdentifier = "searchResultCell"
    let cellReuseIdentifier_recc = "RecommendCollectionCell"
    let cellReuseIdentifier_othc = "OtherCollectionCell"
    
    var dateSelect : Date!
    var galleryAry = [CCater]()
    var filteredGalleryAry = [CCater]()
    var selectedCater: CCater!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearchFilter.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(closeFilterView)))
//        viewSearchFilter.layer.zPosition = 10;
        
        //NotificationCenter.default.addObserver(self, selector: #selector(updateSearchFilterParams), name: NSNotification.Name(rawValue: g_skUpdateSearchFilter), object: nil)
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        tfSelectDate.inputView = datePicker;
        datePicker.addTarget(self, action: #selector(onDate), for: .valueChanged)
        
        SelectCategoryVC.delegate = self
        
        initView()
        fetchGalleries()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func fetchGalleries() {
        ProgressHUD.show("", interaction: false)
        CaterApi.fetchCaters { (caters) in
            ProgressHUD.dismiss()
            if !caters.isEmpty {
                self.galleryAry = caters
//                self.filteredGalleryAry = caters ///
//                self.tblvDiscover.reloadData() ///
                self.totalRecommendLbl.text = "Total " + String(self.galleryAry.count) + " records found"
                self.recmdCollectionView.reloadData()
            }
        }
    }
    
    func initView(){
        tblvDiscover.isHidden = true//false
        viewSearchFilter.isHidden = true
    }
    
    @objc func closeFilterView(){
        viewSearchFilter.isHidden = true
    }
    
    @objc func updateSearchFilterParams(){
        
    }
    
    @IBAction func onDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateSelect = sender.date
        tfSelectDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func onFilter(_ sender: Any) {
        print("onFilter clicked")
        viewSearchFilter.isHidden = false
    }

    @IBAction func onSetting(_ sender: Any) {
        ProgressHUD.showError("Pending!", interaction: false)
    }
    
    @IBAction func onChooseLocation(_ sender: Any) {
        print("onChooseLocation button clicked")
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func onSelectDate(_ sender: Any) {
        // This method is not needed, because tfSelectDate inputview is decided to datepicker
    }
    
    @IBAction func onChooseCategory(_ sender: Any) {
        print("onChooseCategory button clicked")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCategoryVC")
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func onSearch(_ sender: Any) {
        print("Search button clicked")
        closeFilterView()
        tblvDiscover.isHidden = false
        discoverTableview.isHidden = true
        
        //TODO search action, after that
        searchAction()
    }
    
    @IBAction func onClearSearch(_ sender: Any) {
        print("onClearSearch button clicked")
        tfSelectDate.text = ""
        btnChooseLocation.titleLabel?.text = "Choose a Location"
        btnChooseCategory.titleLabel?.text = "Choose Category"
        closeFilterView()
        tblvDiscover.isHidden = true
        discoverTableview.isHidden = false
    }
    
    func searchAction() {
        if filteredGalleryAry.count > 0 {
            filteredGalleryAry.removeAll()
            filteredGalleryAry.append(contentsOf:galleryAry)
            print("filtered gallery count = \(filteredGalleryAry.count)")
        }
        tblvDiscover.reloadData()
    }
    
    func goDetailVC() {
        //save selected cater's id & userid to nsuserdefaults
        let dic = [Config.KEY_ID: selectedCater.id, Config.KEY_USERID: selectedCater.userId, Config.KEY_TYPE: selectedCater.type, Config.KEY_URL_IMAGE: selectedCater.imageUrl]
        g_userDefault.set(dic, forKey: g_skCurrentCater)
        self.performSegue(withIdentifier: "searchToDetailSegue", sender: self)
    }
    
    @IBAction func onRecommendMoreAction(_ sender: Any) {
        ProgressHUD.showError("Pending!", interaction: false)
    }
    
    @IBAction func onOtherMoreAction(_ sender: Any) {
        ProgressHUD.showError("Pending!", interaction: false)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchToDetailSegue" {
            let destinationVC = segue.destination as! DiscoverDetailVC
            destinationVC.mainCater = selectedCater
        }
    }

}

extension DiscoverVC: DiscoverTvCellDelegate {
    func like(_ sender: DiscoverSearchTableViewCell, isLike: Bool) {
        guard let tappedIndexPath = self.tblvDiscover.indexPath(for: sender) else { return }
        let cater = filteredGalleryAry[tappedIndexPath.row]
        if isLike {
            print("like")
            LikeApi.setLike(userId: g_userDefault.string(forKey: g_skCurrentUserKey)!, caterId: cater.id!)
        } else {
            print("unlike")
            LikeApi.setUnlike(userId: g_userDefault.string(forKey: g_skCurrentUserKey)!, caterId: cater.id!)
        }
    }
    
}

extension DiscoverVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredGalleryAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! DiscoverSearchTableViewCell
        
        let cater = filteredGalleryAry[indexPath.row]
        if let photoUrlString = cater.imageUrl {
            let photoUrl = URL(string: photoUrlString)
            cell.itemImage.sd_setImage(with: photoUrl)
        }
        let rateImage = UIImage(named: "icon_star_\(Int(cater.rate!))")
        cell.itemRateImg.image = rateImage
        cell.itemTitle.text = cater.type
        
        let userid = cater.userId
        Api.CUser.observeUser(withId: userid!) { (owner) in
            if owner.institute! != "" &&  owner.experience! != "" {
                cell.itemDetail.text = "$$$ " + owner.institute! + " - " + owner.experience!
            } else {
                cell.itemDetail.text = owner.institute! + owner.experience!
            }
        }
        
        LikeApi.isLike(userId: g_userDefault.string(forKey: g_skCurrentUserKey)!, caterId: cater.id!) { (like) in
            if like {
                if let image = UIImage(named: "icon_star_filled") {
                    cell.itemLikeBtn.setImage(image, for: .normal)
                }
            }
        }
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let currentCell = tableView.cellForRow(at: indexPath) as! DiscoverSearchTableViewCell
        selectedCater = filteredGalleryAry[indexPath.row]
        goDetailVC()
    }
}

extension DiscoverVC: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        btnChooseLocation.titleLabel?.text = place.name
//        btnChooseLocation.setTitleColor(UIColor.black, for: .normal)
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension DiscoverVC: SelectCategoryVCDelegate {
    func selecteCategory(_ ary: [String]) {
        var title : String! = ""
        for category in ary {
            if title  == "" {
                title = title + category
            } else {
                title = title + " ," + category
            }
        }
        btnChooseCategory.titleLabel?.text = title
//        btnChooseCategory.setTitleColor(UIColor.black, for: .normal)
    }
}

extension DiscoverVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView ==  recmdCollectionView {
            return galleryAry.count
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView ==  recmdCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier_recc, for: indexPath) as! DiscoverCollectionViewCell
            
            let cater = galleryAry[indexPath.item]
            if let photoUrlString = cater.imageUrl {
                let photoUrl = URL(string: photoUrlString)
                cell.foodImageView.sd_setImage(with: photoUrl)
            }
            let rateImage = UIImage(named: "icon_star_\(Int(cater.rate!))")
            cell.rateView.image = rateImage
            
            let userid = cater.userId
            Api.CUser.observeUser(withId: userid!) { (owner) in
                cell.lblName.text = owner.username
                cell.lblDetail.text = owner.description
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier_othc, for: indexPath) as! DiscoverCollectionViewCell
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView ==  recmdCollectionView {
            selectedCater = self.galleryAry[indexPath.item]
            goDetailVC()
        }
    }
    
}
