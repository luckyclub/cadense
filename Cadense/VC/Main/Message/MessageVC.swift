//
//  MessageVC.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import AudioToolbox
import SDWebImage

class MessageVC: UIViewController {

    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    let cellReuseIdentifier = "messageCell"
    
    var items = [CConversation]()
    var filteredItems = [CConversation]()
    var selectedUser: CUser!
    static var isSelected: Bool = false
    static var other: CUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchData()
        
        if MessageVC.isSelected {
            print("start chatting !!!")
            self.selectedUser = MessageVC.other
            self.goChattingVC()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Downloads conversations
    func fetchData() {
        CConversation.showConversations { (conversations) in
            self.items = conversations
            self.filteredItems = conversations
            self.filteredItems.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp } //items
            DispatchQueue.main.async {
                self.tableview.reloadData()
                for conversation in self.filteredItems {
                    if conversation.lastMessage.isRead == false {
                        self.playSound()
                        break
                    }
                }
            }
        }
    }
    
    func playSound()  {
        var soundURL: NSURL?
        var soundID:SystemSoundID = 0
        let filePath = Bundle.main.path(forResource: "newMessage", ofType: "wav")
        soundURL = NSURL(fileURLWithPath: filePath!)
        AudioServicesCreateSystemSoundID(soundURL!, &soundID)
        AudioServicesPlaySystemSound(soundID)
    }
    
    func goChattingVC() {
        self.performSegue(withIdentifier: "msgToChatSegue", sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "msgToChatSegue" {
            let vc = segue.destination as! ChattingVC
            vc.currentUser = self.selectedUser
            MessageVC.isSelected = false
        }
    }

}

extension MessageVC: UITableViewDelegate, UITableViewDataSource {
    
    //including event, height 87
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MessageTableViewCell
        
        let user = self.filteredItems[indexPath.row].user
        let lstMsg = self.filteredItems[indexPath.row].lastMessage
        
        if let photoUrlString = user.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            cell.avatarImg.sd_setImage(with: photoUrl)
        }
        
        cell.nameLbl.text = user.username
        
        switch lstMsg.type {
        case .text:
            let message = lstMsg.content as! String
            cell.msgLbl.text = message
        case .location:
            cell.msgLbl.text = "Location"
        default:
            cell.msgLbl.text = "Media"
        }
        
        let messageDate = Date.init(timeIntervalSince1970: TimeInterval(lstMsg.timestamp))
        let dataformatter = DateFormatter.init()
        dataformatter.timeStyle = .short
        let date = dataformatter.string(from: messageDate)
        cell.timeLbl.text = date
        
        if lstMsg.owner == .sender && lstMsg.isRead == false {
            cell.msgLbl.font = UIFont(name:"Raleway-SemiBold", size: 13.0)
            cell.timeLbl.font = UIFont(name:"Raleway-SemiBold", size: 13.0)
            cell.msgLbl.textColor = g_appRedColor
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.filteredItems.count > 0 {
            self.selectedUser = self.filteredItems[indexPath.row].user
            self.goChattingVC()
        }
    }
}

extension MessageVC : UISearchBarDelegate {
    
    func searchBarIsEmpty() -> Bool{
        return self.searchbar.text?.isEmpty ?? true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search button clicked")
        self.searchbar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searhchTextChang:\(searchText)")
        filteredItems.removeAll()
        
        if searchText == "" {
            filteredItems = items
            self.tableview.reloadData()
            return
        }
        
        let myGroup = DispatchGroup()
        for conversation in items {
            myGroup.enter()
            let user = conversation.user
            let lstMsg = conversation.lastMessage
            let username = user.username
            var msg = ""
            switch lstMsg.type {
            case .text:
                let message = lstMsg.content as! String
                msg = message
            case .location:
                msg = "Location"
            default:
                msg = "Media"
            }
            
            if (username?.lowercased().range(of:searchText.lowercased()) != nil || msg.lowercased().range(of:searchText.lowercased()) != nil) {
                filteredItems.append(conversation)
            }
        }
        
        print("filteredResult:\(filteredItems.count)")
        self.tableview.reloadData()
        
        myGroup.notify(queue: .main) {
            print("reload result")
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("search bar started")
        self.searchbar.text = ""
        filteredItems.removeAll()
        return true
    }
    
}
