//
//  UserGalleriesCollectionViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/17/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class UserGalleriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var galleryPhoto: UIImageView!
}
