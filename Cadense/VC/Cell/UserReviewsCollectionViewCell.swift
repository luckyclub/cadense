//
//  UserReviewsCollectionViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/17/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

protocol ReviewClCellDelegate : class {
    func viewFullReview(_ sender: UserReviewsCollectionViewCell)
}

class UserReviewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!
    weak var delegate: ReviewClCellDelegate?
    
    @IBAction func onSeeFullReviewAction(_ sender: Any) {
        self.delegate?.viewFullReview(self)
    }
}
