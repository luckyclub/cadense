//
//  DiscoverTitleTableViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/18/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class DiscoverTitleTableViewCell: UITableViewCell {
    //id: discoverTitleCell
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var countInfoLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onMoreBtnAction(_ sender: Any) {
    }
}
