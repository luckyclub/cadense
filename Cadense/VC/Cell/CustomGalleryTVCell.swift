//
//  CustomGalleryTVCell.swift
//  Cadense
//
//  Created by Lucky on 4/27/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CustomGalleryTVCell: UITableViewCell, UIScrollViewDelegate  {

    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var countView: UIView!
    
    var pageViews: [UIImageView?] = []
    let img_wd: CGFloat! = 110.0
    let img_pd: CGFloat! = 6.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setGalleries(ary: [CCater]) {
        scrollView.contentSize = CGSize(width: (img_wd + img_pd) * CGFloat(ary.count), height:img_wd)
        
        var frame = CGRect(x:img_pd,y:0,width:img_wd,height:img_wd)
        for index in 0..<ary.count {
            frame.origin.x = img_pd + (img_wd + img_pd) * CGFloat(index)
            let imgView = UIImageView(frame: frame)
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            scrollView.addSubview(imgView)
            
            let cater = ary[index]
            if let photoUrlString = cater.imageUrl {
                let photoUrl = URL(string: photoUrlString)
                imgView.sd_setImage(with: photoUrl)
            }
        }
//        self.bringSubview(toFront: countView)
        countView.layer.zPosition = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
