//
//  DiscoverSearchTableViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/17/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

protocol DiscoverTvCellDelegate : class {
    func like(_ sender: DiscoverSearchTableViewCell, isLike: Bool)
}

class DiscoverSearchTableViewCell: UITableViewCell {
    //id: searchResultCell
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDetail: UILabel!
    @IBOutlet weak var itemRateView: UIView!
    @IBOutlet weak var itemRateImg: UIImageView!
    @IBOutlet weak var itemRateTotalNum: UILabel!
    @IBOutlet weak var itemLikeBtn: UIButton!
    weak var delegate: DiscoverTvCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onLikeBtnAction(_ sender: UIButton) {
        if sender.image(for: .normal) == UIImage(named: "icon_star_empty") {
            if let image = UIImage(named: "icon_star_filled") {
                sender.setImage(image, for: .normal)
            }
            self.delegate?.like(self, isLike: true)
        } else {
            if let image = UIImage(named: "icon_star_empty") {
                sender.setImage(image, for: .normal)
            }
            self.delegate?.like(self, isLike: false)
        }
    }
    
}
