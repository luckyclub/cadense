//
//  LeaveReviewTVCell.swift
//  Cadense
//
//  Created by Lucky on 4/23/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import Cosmos

protocol RateViewDelegate : class {
    func finishCosmosRate(_ sender: LeaveReviewTVCell, rate: Int)
}

class LeaveReviewTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    weak var delegate: RateViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rateView.settings.fillMode = .full
        rateView.didTouchCosmos = { rating in            
            self.delegate?.finishCosmosRate(self, rate: Int(rating))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
