//
//  ReviewTableViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/19/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

protocol ReviewCellDelegate : class {
    func viewFullReview(_ sender: ReviewTableViewCell)
}

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!
    @IBOutlet weak var reviewLbl: UILabel!
    weak var delegate: ReviewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        makeRoundImageWithWhiteBorder(avatarImg)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func fullreviewAction(_ sender: Any) {
        self.delegate?.viewFullReview(self)
    }
}
