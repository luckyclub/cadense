//
//  DiscoverCollectionViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/17/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class DiscoverCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var rateView: UIImageView!
}
