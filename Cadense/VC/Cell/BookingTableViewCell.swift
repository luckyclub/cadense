//
//  BookingTableViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/20/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
