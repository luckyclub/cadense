//
//  ChooseCategoryTableViewCell.swift
//  Cadense
//
//  Created by Lucky on 4/17/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class ChooseCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var isCheckedImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        let isChecked: Bool? = self.isCheckedImage?.isHidden
//        if  isChecked! {
//            isCheckedImage.isHidden = false
//        } else {
//            isCheckedImage.isHidden = true
//        }
        
    }

}
