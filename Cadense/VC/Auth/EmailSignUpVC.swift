//
//  EmailSignUpVC.swift
//  Cadense
//
//  Created by Lucky on 23/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class EmailSignUpVC: UIViewController {
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfBirthday: UITextField!
    
    @IBOutlet weak var btnGetUpdate: UIButton!
    @IBOutlet weak var btnAgree: UIButton!
    
    var bIsAgreed = false
    var bGetUpdate = false
    var dateBirth : Date!
    override func viewDidLoad() {
        super.viewDidLoad()

        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        tfBirthday.inputView = datePicker;
        datePicker.addTarget(self, action: #selector(onBirthday), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func onTap(){
        view.endEditing(true)
    }
    
    @IBAction func onBirthday(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateBirth = sender.date
        tfBirthday.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func onGetUpdate(_ sender: Any) {
        bGetUpdate = !bGetUpdate
        if bGetUpdate{
            btnGetUpdate.setImage(UIImage(named: "icon_checked_white"), for: .normal)
        }else{
            btnGetUpdate.setImage(UIImage(named: "icon_unchecked_white"), for: .normal)
        }
    }
    
    @IBAction func onAgree(_ sender: Any) {
        bIsAgreed = !bIsAgreed
        if bIsAgreed{
            btnAgree.setImage(UIImage(named: "icon_checked_white"), for: .normal)
        }else{
            btnAgree.setImage(UIImage(named: "icon_unchecked_white"), for: .normal)
        }
    }
    
    @IBAction func onDone(_ sender: Any) {
        if (tfName.text?.isEmpty)! || (tfEmail.text?.isEmpty)! || (tfPassword.text?.isEmpty)! || (tfBirthday.text?.isEmpty)!{
            showOkAlert(title: "Warning", msg: "Please fill out the required fields.", vc: self)
            return
        }
        
        if !validateEmail(email: tfEmail.text!){
            showOkAlert(title: "Warning", msg: "Please enter a valid email address.", vc: self)
            tfEmail.becomeFirstResponder()
            return
        }
        
        if !bIsAgreed {
            showOkAlert(title: "Warning", msg: "You need agree to terms and conditions.", vc: self)
            return
        }
        
        ProgressHUD.show("Creating Account", interaction: false)
        
        AuthService.signUp(username: tfName.text!, email: tfEmail.text!, password: tfPassword.text!, birthday: dateBirth, onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.navigationController?.popViewController(animated: true)
        }) { (errorString) in
            ProgressHUD.showError(errorString!)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension EmailSignUpVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfName {
            tfEmail.becomeFirstResponder()
        }else if textField == tfEmail {
            tfPassword.becomeFirstResponder()
        }else if textField == tfPassword {
            tfBirthday.becomeFirstResponder()
        }else{
            view.endEditing(true)
        }
        
        return true
    }
}
