//
//  HelpVC.swift
//  Cadense
//
//  Created by Lucky on 23/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if g_userDefault.bool(forKey: g_skStayLogin) && Api.CUser.CURRENT_USER != nil{
            self.performSegue(withIdentifier: "segueMain", sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
