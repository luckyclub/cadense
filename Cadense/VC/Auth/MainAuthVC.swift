//
//  MainAuthVC.swift
//  Cadense
//
//  Created by Lucky on 23/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class MainAuthVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func fbButtonClicked(_ sender: Any) {
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    showOkAlert(title: "Login error", msg: error.localizedDescription, vc: self)
                    return
                }
                
                // Present the main view
                self.performSegue(withIdentifier: "segueMain", sender: nil)
            })
            
        }
    }

}
