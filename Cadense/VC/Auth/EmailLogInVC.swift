//
//  EmailLogIn.swift
//  Cadense
//
//  Created by Lucky on 23/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class EmailLogInVC: UIViewController {
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var btnStayLogin: UIButton!
    
    var bIsLogin = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        view.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func onTap(){
        view.endEditing(true)
    }
    
    @IBAction func onLogin(_ sender: Any) {
        if (tfEmail.text?.isEmpty)! || (tfPassword.text?.isEmpty)! {
            showOkAlert(title: "Warning", msg: "Please fill out the required fields.", vc: self)
            return
        }
        
        if !validateEmail(email: tfEmail.text!){
            showOkAlert(title: "Warning", msg: "Please enter a valid email address.", vc: self)
            tfEmail.becomeFirstResponder()
            return
        }
        
        ProgressHUD.show("Log In", interaction: false)
        AuthService.signIn(email: tfEmail.text!, password: tfPassword.text!, onSuccess: {
            ProgressHUD.showSuccess("Success")
            g_userDefault.set(self.bIsLogin, forKey: g_skStayLogin)
            self.performSegue(withIdentifier: "segueMain", sender: nil)            
        }, onError: { error in
            ProgressHUD.showError(error!)
        })
    }
    
    @IBAction func onStayLogin(_ sender: Any) {
        bIsLogin = !bIsLogin
        if bIsLogin{
            btnStayLogin.setImage(UIImage(named: "icon_checked_white"), for: .normal)
        }else{
            btnStayLogin.setImage(UIImage(named: "icon_unchecked_white"), for: .normal)
        }
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension EmailLogInVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {
            tfPassword.becomeFirstResponder()
        }else{
            view.endEditing(true)
        }
        
        return true
    }
}
