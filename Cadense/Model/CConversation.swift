//
//  CConversation.swift
//  Cadense
//
//  Created by Lucky on 5/6/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class CConversation {
    
    //MARK: Properties
    let user: CUser
    var lastMessage: CMessage
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([CConversation]) -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [CConversation]()
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: String]
                    let location = values["location"]!
                    Api.CUser.observeUser(withId: fromID, completion: { (user) in
                        let emptyMessage = CMessage.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true)
                        let conversation = CConversation.init(user: user, lastMessage: emptyMessage)
                        conversations.append(conversation)
                        conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                            completion(conversations)
                        })
                    })
                }
            })
        }
    }
    
    //MARK: Inits
    init(user: CUser, lastMessage: CMessage) {
        self.user = user
        self.lastMessage = lastMessage
    }
}
