//
//  User.swift
//  Cadense
//
//  Created by Lucky on 24/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation

class CUser {
    var email: String?
    var profileImageUrl: String?
    var username: String?
    var id: String?
    var birthday: Date?
//    var isFollowing: Bool?
    
    var description: String?
    var institute: String?
    var experience: String?
    var certification: String?
    var hourlyRate: String?
    var address: String?
    var location: String?
    var category: [String]?
}

extension CUser {
    static func transformUser(dict: [String: Any], key: String) -> CUser {
        let user = CUser()
        user.id = key
        user.email = dict["email"] as? String
        user.profileImageUrl = dict[Config.KEY_URL_AVATAR] as? String
        user.username = dict["username"] as? String
        if let dBirthday = dict["birthday"] as? Double {
            user.birthday = Date(timeIntervalSince1970: dBirthday)
        }
        
        user.description = dict["description"] as? String ?? ""
        user.institute = dict["institute"] as? String ?? ""
        user.experience = dict["experience"] as? String ?? ""
        user.certification = dict["certifications"] as? String ?? ""
        user.hourlyRate = dict["hourlyRate"] as? String ?? ""
        user.category = dict["category"] as? [String] ?? nil
        
        user.address = dict["address"] as? String ?? ""
        user.location = dict["location"] as? String ?? "0,0"
        
        return user
    }
}
