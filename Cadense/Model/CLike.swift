//
//  CLike.swift
//  Cadense
//
//  Created by Lucky on 4/26/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CLike {
    var id: String?
    var userId: String?
    var caterId: String?
}

extension CLike {
    static func transformLike(dict: [String: Any], key: String) -> CLike {
        let like = CLike()
        like.id = key
        like.userId = dict[Config.KEY_USERID] as? String
        like.caterId = dict[Config.KEY_CATERID] as? String
        
        return like
    }
}
