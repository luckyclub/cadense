//
//  CReview.swift
//  Cadense
//
//  Created by Lucky on 4/26/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CReview {
    var id: String?
    var caterId: String?
    var bookingId: String?
    var clientId: String?
    var userId: String?
    var caterImageUrl: String?
    var rate: [Int]?
    var publicFeedback: String?
    var privateFeedback: String?
    var protectFeedback: String?
    var date: Date?
}

extension CReview {
    static func transformReview(dict: [String: Any], key: String) -> CReview {
        let review = CReview()
        review.id = key
        review.caterId = dict[Config.KEY_CATERID] as? String
        review.clientId = dict[Config.KEY_CLIENTID] as? String
        review.userId = dict[Config.KEY_USERID] as? String
        review.bookingId = dict[Config.KEY_BOOKID] as? String
        review.caterImageUrl = dict[Config.KEY_URL_IMAGE] as? String
        review.rate = dict[Config.KEY_RATE] as? [Int]
        if let dDate = dict[Config.KEY_DATE] as? Double {
            review.date = Date(timeIntervalSince1970: dDate)
        }
        review.publicFeedback = dict[Config.KEY_FD_PUBLIC] as? String
        review.privateFeedback = dict[Config.KEY_FD_PRIVATE] as? String
        review.protectFeedback = dict[Config.KEY_FD_PROTECT] as? String
        
        return review
    }
}
