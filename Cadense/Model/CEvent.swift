//
//  CEvent.swift
//  Cadense
//
//  Created by Lucky on 4/28/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CEvent {
    var id: String?
    var userId: String?
    var type: String?
}

extension CEvent {
    static func transformEvent(dict: [String: Any], key: String) -> CEvent {
        let event = CEvent()
        event.id = key
        event.userId = dict[Config.KEY_USERID] as? String
        event.type = dict[Config.KEY_TYPE] as? String
        
        return event
    }
}
