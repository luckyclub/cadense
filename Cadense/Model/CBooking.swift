//
//  CBooking.swift
//  Cadense
//
//  Created by Lucky on 4/26/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CBooking {
    var id: String?
    var caterId: String?
    var clientId: String?
    var userId: String?
    var caterImageUrl: String?
    var isConfirm: Bool?
    var date: Date?
    var duration: String?
    var price: String?
    var type: String?
}

extension CBooking {
    static func transformBook(dict: [String: Any], key: String) -> CBooking {
        let book = CBooking()
        book.id = key
        book.caterId = dict[Config.KEY_CATERID] as? String
        book.clientId = dict[Config.KEY_CLIENTID] as? String
        book.userId = dict[Config.KEY_USERID] as? String
        book.caterImageUrl = dict[Config.KEY_URL_IMAGE] as? String
        book.isConfirm = dict[Config.KEY_ISCONFIRM] as? Bool ?? false
        if let dDate = dict[Config.KEY_DATE] as? Double {
            book.date = Date(timeIntervalSince1970: dDate)
        }
        book.duration = dict[Config.KEY_DURATION] as? String
        book.price = dict[Config.KEY_PRICE] as? String
        book.type = dict[Config.KEY_TYPE] as? String
        
        return book
    }
}
