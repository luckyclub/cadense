//
//  CCater.swift
//  Cadense
//
//  Created by Lucky on 4/26/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit

class CCater {
    var id: String?
    var imageUrl: String?
    var userId: String?
    var rate: Float?
    var type: String?
}

extension CCater {
    static func transformCater(dict: [String: Any], key: String) -> CCater {
        let cater = CCater()
        cater.id = key
        cater.imageUrl = dict[Config.KEY_URL_IMAGE] as? String
        cater.userId = dict[Config.KEY_USERID] as? String
        cater.rate = dict[Config.KEY_RATE] as? Float ?? 0
        cater.type = dict[Config.KEY_TYPE] as? String
        
        return cater
    }
}
