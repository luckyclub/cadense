//
//  SearchFilter.swift
//  Cadense
//
//  Created by Lucky on 04/04/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import CoreLocation

class SearchFilter{
    var date : Date
    var category : Array<Any> = []
    var locationRange : String
    var street : String
    var city : String
    var country : String
    var location : CLLocation
    
    static let shared = SearchFilter()
    
    init() {
        date = Date()
        category = []
        locationRange = ""
        street = ""
        city = ""
        country = ""
        location = CLLocation()
    }
}
