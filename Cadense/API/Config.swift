//
//  Config.swift
//  Cadense
//
//  Created by Lucky on 24/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation

struct Config {
    static var STORAGE_ROOF_REF = "gs://cadense-cf7f7.appspot.com"
    
    //firebase nodes
    static let KEY_NODE_USER     : String = "users"
    static let KEY_NODE_CATER    : String = "caters"
    static let KEY_NODE_EVENT    : String = "events"
    static let KEY_NODE_BOOK     : String = "books"
    static let KEY_NODE_REVIEW   : String = "reviews"
    static let KEY_NODE_LIKE     : String = "likes"
    //firebase storage
    static let KEY_STORE_CATER     : String = "cater_image"
    static let KEY_STORE_AVATAR    : String = "profile_image"
    
    //keys
    static let KEY_ID           : String = "id"
    static let KEY_USERID       : String = "userId"
    static let KEY_CATERID      : String = "caterId"
    static let KEY_CLIENTID     : String = "clientId"
    static let KEY_BOOKID       : String = "bookingId"
    static let KEY_TYPE         : String = "type"
    static let KEY_URL_IMAGE    : String = "imageUrl"
    static let KEY_URL_AVATAR   : String = "profileImageUrl"
    static let KEY_ISCONFIRM    : String = "isConfirm"
    static let KEY_RATE         : String = "rate"
    static let KEY_DATE         : String = "date"
    static let KEY_DURATION     : String = "duration"
    static let KEY_PRICE        : String = "price"
    static let KEY_FD_PUBLIC    : String = "publicFeedback"
    static let KEY_FD_PRIVATE   : String = "privateFeedback"
    static let KEY_FD_PROTECT   : String = "protectFeedback"
}
