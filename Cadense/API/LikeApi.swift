//
//  LikeApi.swift
//  Cadense
//
//  Created by Lucky on 5/3/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class LikeApi {
    //API: save Like
    static func setLike(userId: String, caterId: String) {
        let refLikes: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_LIKE)
        let dict = [caterId:true] as [String : Any]
        refLikes.child(userId).updateChildValues(dict)
    }
    
    //API: unlike
    static func setUnlike(userId: String, caterId: String) {
        let refLikes: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_LIKE)
        refLikes.child(userId).child(caterId).setValue(nil)
    }
    
    //API: check like or unlike
    static func isLike(userId: String, caterId: String, completion: @escaping (Bool) -> Void) {
        let refLikes: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_LIKE)
        refLikes.child(userId).observe(.value, with: {
            snapshot in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let key = item.key
                    if key == caterId {
                        completion(true)
                        break
                    }
                }
                completion(false)
            } else {
                completion(false)
            }
        })
    }
    
    //API: get liked caters by userId
    static func fetchLikesByUserId(id: String, completion: @escaping ([String]) -> Void) {
        var likes = [String]()
        let refLikes: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_LIKE)
        refLikes.child(id).observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let key = item.key
                    likes.append(key)
                }
            }
            completion(likes)
        })
    }
}
