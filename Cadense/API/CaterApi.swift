//
//  CaterApi.swift
//  Cadense
//
//  Created by Lucky on 4/27/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage

class CaterApi {
    
    //API: save cater
    static func setCater(imageData: Data, userId: String, type: String , onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let refCaters: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_CATER)
        let key = refCaters.childByAutoId().key
        
        let storageRef = Storage.storage().reference(forURL: Config.STORAGE_ROOF_REF).child(Config.KEY_STORE_CATER).child(key)
        storageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
            if error != nil {
                return
            }
            let imageUrl: String = (metadata?.downloadURL()?.absoluteString)!
            let dict = [Config.KEY_URL_IMAGE: imageUrl, Config.KEY_USERID: userId, Config.KEY_TYPE: type, Config.KEY_RATE: 0] as [String : Any]
            refCaters.child(key).setValue(dict, withCompletionBlock: { (error, ref) in
                if error != nil {
                    onError(error!.localizedDescription)
                } else {
                    onSuccess()
                }
            })
        })
    }
    
    //API: get caters
    static func fetchCaters(completion: @escaping ([CCater]) -> Void) {
        var caters = [CCater]()
        let refCaters = Database.database().reference().child(Config.KEY_NODE_CATER)
        refCaters.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let cater = CCater.transformCater(dict: dict!, key: item.key)
                    caters.append(cater)
                }
            }
            completion(caters)
        })
    }
    
    //get caters with user info
    //this is not used and not perfect
    static func fetchCatersWithUserInfo(completion: @escaping ([[String: Any]]) -> Void) {
        var caters = [[String: Any]]()
        let refCaters = Database.database().reference().child(Config.KEY_NODE_CATER)
        refCaters.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let cater = CCater.transformCater(dict: dict!, key: item.key)
                    let userid = cater.userId
                    Api.CUser.observeUser(withId: userid!, completion: { (user) in
                        let newDict = ["cater": cater, "user": user] as [String : Any]
                        caters.append(newDict)
                    })
                }
            }
            completion(caters)
        })
    }
    
    //API: get caters by userId
    static func fetchCatersByUserId(id: String, completion: @escaping ([CCater]) -> Void) {
        var caters = [CCater]()
        let refCaters = Database.database().reference().child(Config.KEY_NODE_CATER).queryOrdered(byChild: Config.KEY_USERID).queryEqual(toValue : id)
        refCaters.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let cater = CCater.transformCater(dict: dict!, key: item.key)
                    caters.append(cater)
                }
            }
            completion(caters)
        })
    }
    
    //update confirm flag value
    static func updateCaterRate(id: String, rate: Int) {
        let refCaters = Database.database().reference().child(Config.KEY_NODE_CATER).child(id)
        refCaters.updateChildValues([Config.KEY_RATE: rate])
    }
    
    //delete cater by id
    static func removeCaterById(id: String) {
        let refCaters: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_CATER)
        refCaters.child(id).setValue(nil)
    }
}
