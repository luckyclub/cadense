//
//  BookingApi.swift
//  Cadense
//
//  Created by Lucky on 5/3/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage

class BookingApi {
    //API: save Book
    static func setBook(dict: [String : Any] , onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let refBooks: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_BOOK)
        let key = refBooks.childByAutoId().key
        
        refBooks.child(key).setValue(dict, withCompletionBlock: { (error, ref) in
            if error != nil {
                onError(error!.localizedDescription)
            } else {
                onSuccess()
            }
        })
    }
    
    //get books by userId
    static func fetchBooksByUserId(id: String, completion: @escaping ([CBooking]) -> Void) {
        var books = [CBooking]()
        let refBooks = Database.database().reference().child(Config.KEY_NODE_BOOK).queryOrdered(byChild: Config.KEY_USERID).queryEqual(toValue : id)
        refBooks.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let book = CBooking.transformBook(dict: dict!, key: item.key)
                    books.append(book)
                }
            }
            completion(books)
        })
    }
    
    //update confirm flag value
    static func updateBookConfirmFlag(id: String, onSuccess: @escaping () -> Void) {
        let refBooks = Database.database().reference().child(Config.KEY_NODE_BOOK)
        let newBookRef = refBooks.child(id)
        newBookRef.updateChildValues([Config.KEY_ISCONFIRM: true])
        onSuccess()
    }
    
    //delete book by id
    static func removeBookById(id: String) {
        let refBooks: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_BOOK)
        refBooks.child(id).setValue(nil)
    }
    
}
