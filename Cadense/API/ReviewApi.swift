//
//  ReviewApi.swift
//  Cadense
//
//  Created by Lucky on 5/3/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class ReviewApi {
    //API: save Review
    static func setReview(dict: [String : Any] , onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let refReviews: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_REVIEW)
        let key = refReviews.childByAutoId().key
        
        refReviews.child(key).setValue(dict, withCompletionBlock: { (error, ref) in
            if error != nil {
                onError(error!.localizedDescription)
            } else {
                let caterId = dict[Config.KEY_CATERID] as! String
                self.fetchReviewsByCaterId(id: caterId, completion: { (reviews) in
                    var avRate: Int = 0
                    let myGroup = DispatchGroup()
                    if reviews.count > 0 {
                        for review in reviews {
                            myGroup.enter()
                            let rate: Int! = review.rate!.last
                            avRate += rate
                        }
                        avRate = Int(avRate / reviews.count)
                        if avRate >= 5 {
                            avRate = 5
                        }
                        CaterApi.updateCaterRate(id: caterId, rate: avRate)
                    }
                })
                onSuccess()
            }
        })
    }
    
    //API: get Reviews by clientId
    static func fetchReviewsByClientId(id: String, completion: @escaping ([CReview]) -> Void) {
        var reviews = [CReview]()
        let refBooks = Database.database().reference().child(Config.KEY_NODE_REVIEW).queryOrdered(byChild: Config.KEY_CLIENTID).queryEqual(toValue : id)
        refBooks.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let review = CReview.transformReview(dict: dict!, key: item.key)
                    reviews.append(review)
                }
            }
            completion(reviews)
        })
    }
    
    //get Reviews by caterId
    static func fetchReviewsByCaterId(id: String, completion: @escaping ([CReview]) -> Void) {
        var reviews = [CReview]()
        let refBooks = Database.database().reference().child(Config.KEY_NODE_REVIEW).queryOrdered(byChild: Config.KEY_CATERID).queryEqual(toValue : id)
        refBooks.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let review = CReview.transformReview(dict: dict!, key: item.key)
                    reviews.append(review)
                }
            }
            completion(reviews)
        })
    }
    
    //delete review by id
    static func removeReviewById(id: String) {
        let refReviews: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_REVIEW)
        refReviews.child(id).setValue(nil)
    }
}
