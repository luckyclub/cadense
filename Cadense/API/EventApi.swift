//
//  EventApi.swift
//  Cadense
//
//  Created by Lucky on 4/28/18.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage

class EventApi {
    //API: save Event
    static func setEvent(userId: String, type: String , onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let refEvents: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_EVENT)
        let key = refEvents.childByAutoId().key
        
        let dict = [Config.KEY_USERID: userId, Config.KEY_TYPE: type] as [String : Any]
        refEvents.child(key).setValue(dict, withCompletionBlock: { (error, ref) in
            if error != nil {
                onError(error!.localizedDescription)
            } else {
                onSuccess()
            }
        })
    }
    
    //API: get events by userId
    static func fetchEventsByUserId(id: String, completion: @escaping ([CEvent]) -> Void) {
        var events = [CEvent]()
        let refEvents = Database.database().reference().child(Config.KEY_NODE_EVENT).queryOrdered(byChild: Config.KEY_USERID).queryEqual(toValue : id)
        refEvents.observe(.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    let dict = item.value as? [String: Any]
                    let event = CEvent.transformEvent(dict: dict!, key: item.key)
                    events.append(event)
                }
            }
            completion(events)
        })
    }
    
    //delete event by id
    static func removeEventById(id: String) {
        let refEvents: DatabaseReference! = Database.database().reference().child(Config.KEY_NODE_EVENT)
        refEvents.child(id).setValue(nil)
    }
}
