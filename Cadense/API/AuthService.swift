//
//  AuthService.swift
//  Cadense
//
//  Created by Lucky on 24/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class AuthService {
    
    ///API:SignIn
    static func signIn(email: String, password: String, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user: User?, error: Error?) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            let uid = user?.uid
            g_userDefault.set(uid, forKey: g_skCurrentUserKey)
            onSuccess()
        })
        
    }
    
    ///API:SignUp
    static func signUp(username: String, email: String, password: String, birthday:Date, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user: User?, error: Error?) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            let uid = user?.uid
            let dBirthday = birthday.timeIntervalSince1970
            setUserInfomation(username: username, email: email, uid: uid!, birthday: dBirthday, onSuccess: onSuccess)
        })
        
    }
    
    //When SignUp, save username, username_lowercase, birthday
    static func setUserInfomation(username: String, email: String, uid: String, birthday: Double, onSuccess: @escaping () -> Void) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users")
        let newUserReference = usersReference.child(uid)
        newUserReference.updateChildValues(["username": username, "username_lowercase": username.lowercased(), "email": email, "birthday": birthday])
        onSuccess()
    }
    
    ///API: save category
    static func setUserCategory(category: [String], uid: String, onSuccess: @escaping () -> Void) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users")
        let newUserReference = usersReference.child(uid)
        newUserReference.updateChildValues(["category": category])
        onSuccess()
    }
    
    ///API: update email
    static func updateUserEmail(email: String, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        Api.CUser.CURRENT_USER?.updateEmail(to: email, completion: { (error) in
            if error != nil {
                onError(error!.localizedDescription)
            }else {
                onSuccess()
            }
        })
    }
    
    ///API:update user avatar image
    static func updateUserAvatar(imageData: Data, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        
        let uid = Api.CUser.CURRENT_USER?.uid
        let storageRef = Storage.storage().reference(forURL: Config.STORAGE_ROOF_REF).child(Config.KEY_STORE_AVATAR).child(uid!)
        
        storageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
            if error != nil {
                return
            }
            let profileImageUrl: String = (metadata?.downloadURL()?.absoluteString)!
            Api.CUser.REF_CURRENT_USER?.updateChildValues([Config.KEY_URL_AVATAR: profileImageUrl], withCompletionBlock: { (error, ref) in
                if error != nil {
                    onError(error!.localizedDescription)
                } else {
                    onSuccess()
                }
            })
        })
    }
    
    ///API:update user infomations
    static func updateUserInfo(dict: [String: String], onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        Api.CUser.REF_CURRENT_USER?.updateChildValues(dict, withCompletionBlock: { (error, ref) in
            if error != nil {
                onError(error!.localizedDescription)
            } else {
                onSuccess()
            }
        })
    }    
    
    /******* these methods are doesn't use ********/
    
    //API:update user avatar image and info
    static func updateUserAvatarAndInfo(username: String, email: String, imageData: Data, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        
        let uid = Api.CUser.CURRENT_USER?.uid
        let storageRef = Storage.storage().reference(forURL: Config.STORAGE_ROOF_REF).child("profile_image").child(uid!)
        
        storageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
            if error != nil {
                return
            }
            let profileImageUrl = metadata?.downloadURL()?.absoluteString
            
            self.updateDatabase(profileImageUrl: profileImageUrl!, username: username, email: email, onSuccess: onSuccess, onError: onError)
        })
    }
    
    //Use this when save avatar image
    static func updateDatabase(profileImageUrl: String, username: String, email: String, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let dict = ["username": username, "username_lowercase": username.lowercased(), "email": email, Config.KEY_URL_AVATAR: profileImageUrl]
        Api.CUser.REF_CURRENT_USER?.updateChildValues(dict, withCompletionBlock: { (error, ref) in
            if error != nil {
                onError(error!.localizedDescription)
            } else {
                onSuccess()
            }
            
        })
    }
    
    //API:Logout
    static func logout(onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        do {
            try Auth.auth().signOut()
            onSuccess()
            
        } catch let logoutError {
            onError(logoutError.localizedDescription)
        }
    }
    
    ///API: Delete user
    static func delete(onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        Api.CUser.CURRENT_USER?.delete { error in
            if error != nil {
                onError(error?.localizedDescription)
            } else {
                 onSuccess()
            }
        }
        /*
        let user = Auth.auth().currentUser
        let credential: AuthCredential?
        // Prompt the user to re-provide their sign-in credentials
        user?.reauthenticate(with: credential) { error in
            if error != nil {
                // An error happened.
            } else {
                // User re-authenticated.
            }
        } */
    }
}
