//
//  API
//  Cadense
//
//  Created by Lucky on 24/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
struct Api {
    static var CUser = UserApi()
    static var CCater = CaterApi() // no need
    static var CEvent = EventApi() //no need
}
