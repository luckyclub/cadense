//
//  Global.swift
//  Cadense
//
//  Created by Lucky on 23/03/2018.
//  Copyright © 2018 Alisa. All rights reserved.
//

import Foundation
import UIKit

//not used
let g_appMainColor = UIColor(red: 1, green: 88/255.0, blue: 56/255.0, alpha: 1)
let g_appTextColor = UIColor(red: 70/255.0, green: 78, blue: 104/255.0, alpha: 1.0)
let g_appRedColor = UIColor(red: 233, green: 65/255.0, blue: 92/255.0, alpha: 1.0)
let g_appThirdColor = UIColor(red: 104, green: 222/255.0, blue: 176/255.0, alpha: 1.0)

let g_userDefault = UserDefaults.standard
let g_skStayLogin = "keyStayLogIn"
let g_skCurrentUserKey = "keyCurrentUser"
let g_skCurrentCater = "keyCurrentCater"

let g_skUpdateSearchFilter = "UpdateSearchFilterParams"
let g_searchFilter = SearchFilter.shared
let g_showMessage = "showUserMessages"

enum ShowExtraView {
    case contacts
    case profile
    case preview
    case map
}

enum MessageType {
    case photo
    case text
    case location
}

enum MessageOwner {
    case sender
    case receiver
}

//not used
func setPaddingFor(textField:UITextField){
    let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 5, height: textField.frame.size.height))
    textField.leftView = paddingView
    textField.leftViewMode = .always
}

func showOkAlert(title: String, msg: String, vc:UIViewController){
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    vc.present(alert, animated: true, completion: nil)
}

func validateEmail(email: String)->Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
    return emailTest.evaluate(with:email)
}

func goMainTabbarVC(_ index: Int) {
    var mainView: UIStoryboard!
    mainView = UIStoryboard(name: "Main", bundle: nil)
    let viewcontroller = mainView.instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
    UIApplication.shared.keyWindow?.rootViewController = viewcontroller
    viewcontroller.selectedIndex = index
}

func makeRoundImageWithGrayBorder(_ img: UIImageView) {
    img.layer.borderWidth = 1
    img.layer.masksToBounds = true
    img.layer.borderColor = UIColor.lightGray.cgColor
    img.layer.cornerRadius = img.frame.height/2
}

func makeRoundImageWithWhiteBorder(_ img: UIImageView) {
    img.layer.borderWidth = 1
    img.layer.masksToBounds = true
    img.layer.borderColor = UIColor.white.cgColor
    img.layer.cornerRadius = img.frame.height/2
}

func customSplitDate(date: Date, option: Int) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMM YYYY,dd"
    let dateStr = dateFormatter.string(from: date)
    let dateStrArr = dateStr.components(separatedBy: ",")
    if option == 0 {
        return dateStrArr[0]
    } else {
        return dateStrArr[1]
    }
}

class RoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
